export class Credentials {
  public shortName: string;
  public password: string;

  constructor() {
  }
}

export class Token {
  public LehrerID: string;
  public Token: string;
  public Timestamp: string;

  constructor() {
  }
}
