export class Lesson {
  ID: number;
  Raum: string;
  Datum: Date;
  FaecherID: string;
  KlassenID: string;
  VonUhrzeit: string;
  BisUhrzeit: string;
  classTeacher: boolean;
  courseLeader: boolean;
}
