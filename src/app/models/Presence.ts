export class Presence {
  public ID: number;
  public SchuelerID: number;
  public Vorname: string;
  public Name: string;
  public Fehlzeit: number;
  public excused: boolean;
  public maxTime: number;
  public classTeacher: boolean;
  public courseLeader: boolean;
}
