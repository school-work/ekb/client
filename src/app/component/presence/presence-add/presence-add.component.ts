import {Component, OnInit} from '@angular/core';
import {PresenceService} from '../../../service/presence.service';
import {HandleError} from '../../../helper/handleError';
import {Presence} from '../../../models/Presence';
import {HttpErrorResponse} from '@angular/common/http';
import {AuthService} from '../../../service/auth.service';

@Component({
  selector: 'app-presence-add',
  templateUrl: './presence-add.component.html',
  styles: []
})
export class PresenceAddComponent extends HandleError implements OnInit {

  public presences: Presence[] = [];

  constructor(private presenceService: PresenceService, public auth: AuthService) {
    super();
  }

  ngOnInit() {
    this.getPresencelist();
  }

  public getPresencelist() {
    this.presences = [];
    this.presenceService.getPresenceList().subscribe(data => {
      this.presences = data['data'];
    }, error => {
      this.handleError(error);
    });
  }

  public onChange(model: Presence) {
    const BreakException = {};
    const element = document.getElementById('time-' + model.ID);

    if (this.isValid(model)) {
      element.classList.remove('is-invalid');
    } else {
      element.classList.add('is-invalid');
    }

    try {
      this.presences.forEach(value => {
        if (!this.isValid(value)) {
          document.getElementById('submit').setAttribute('disabled', '');
          throw BreakException;
        } else {
          document.getElementById('submit').removeAttribute('disabled');
        }
      });
    } catch (e) {
      if (e !== BreakException) {
        throw e;
      }
    }
  }

  public saveList() {
    this.submitRequest = false;
    this.errorString = '';
    this.presenceService.savePresenceEntry(this.presences).subscribe(data => {
      this.submitRequest = true;
      this.getPresencelist();
    }, error1 => {
      this.handleError(error1);
    });
  }

  private isValid(model: Presence): boolean {
    return !(model.Fehlzeit == null || model.Fehlzeit > model.maxTime);
  }

  protected handleError(error: HttpErrorResponse) {
    const errorMessage = error.error.data['error'];

    if (error.status === 400) {
      if (errorMessage === 'fehlzeit-not-valid') {
        this.errorString = 'Eine der angegebenen Fehlzeiten ist ungültig!';
      } else if (errorMessage === 'lesson-not-found') {
        this.errorString = 'Eine der Stunden wurde nicht gefunden!';
      }
    } else if (error.status === 401) {
      if (errorMessage === 'not-allowed') {
        this.errorString = 'Sie sind scheinbar nicht dazu berechtigt, eine der Stunden zu bearbeiten!';
      }
    }

    super.handleError(error);
  }
}
