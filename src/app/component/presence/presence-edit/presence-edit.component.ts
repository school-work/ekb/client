import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs/Subscription';
import {LessonService} from '../../../service/lesson.service';
import {HandleError} from '../../../helper/handleError';
import {Lesson} from '../../../models/Lesson';
import {PresenceService} from '../../../service/presence.service';
import {Presence} from '../../../models/Presence';
import {AuthService} from '../../../service/auth.service';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-presence-edit',
  templateUrl: './presence-edit.component.html',
  styles: []
})
export class PresenceEditComponent extends HandleError implements OnInit, OnDestroy {

  private sub: Subscription;
  private identifier: string;
  public lesson: Lesson;
  public presences: Presence[] = [];

  constructor(private router: ActivatedRoute, private lessonService: LessonService, private presenceService: PresenceService,
              public auth: AuthService) {
    super();
  }

  ngOnInit() {
    this.sub = this.router.params.subscribe(value => this.identifier = value['id']);
    this.getLesson();
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  private getLesson() {
    this.lessonService.getLessonByIdentifier(this.identifier).subscribe(value => {
      this.lesson = value['data'];
      this.getPresenceList();
    }, error1 => {
      this.handleError(error1);
    });
  }

  private getPresenceList() {
    this.presences = [];
    this.presenceService.getPresenceListByLessonId(this.identifier).subscribe(value => {
      this.presences = value['data'];

      this.presences.forEach(value1 => {
        if (value1['Entschuldigt'] == 1) {
          value1.excused = true;
        } else {
          value1.excused = false;
        }
      });
    }, error1 => {
      this.handleError(error1);
    });
  }

  public onChange(model: Presence) {
    const BreakException = {};
    const element = document.getElementById('time-' + model.ID);

    if (this.isValid(model)) {
      element.classList.remove('is-invalid');
    } else {
      element.classList.add('is-invalid');
    }

    try {
      this.presences.forEach(value => {
        if (!this.isValid(value)) {
          document.getElementById('submit').setAttribute('disabled', '');
          throw BreakException;
        } else {
          document.getElementById('submit').removeAttribute('disabled');
        }
      });
    } catch (e) {
      if (e !== BreakException) {
        throw e;
      }
    }
  }

  public saveList() {
    this.submitRequest = false;
    this.errorString = '';

    this.presences.forEach(value1 => {
      if (value1.excused) {
        value1['Entschuldigt'] = 1;
      } else {
        value1['Entschuldigt'] = 0;
      }
    });

    this.presenceService.savePresenceEntry(this.presences).subscribe(data => {
      this.submitRequest = true;
      this.getPresenceList();
    }, error1 => {
      this.handleError(error1);
    });
  }

  private isValid(model: Presence): boolean {
    return !(model.Fehlzeit == null || model.Fehlzeit > model.maxTime);
  }

  protected handleError(error: HttpErrorResponse) {
    const errorMessage = error.error.data['error'];

    if (error.status === 400) {
      if (errorMessage === 'fehlzeit-not-valid') {
        this.errorString = 'Eine der angegebenen Fehlzeiten ist ungültig!';
      } else if (errorMessage === 'lesson-not-found') {
        this.errorString = 'Eine der Stunden wurde nicht gefunden!';
      }
    } else if (error.status === 401) {
      if (errorMessage === 'not-allowed') {
        this.errorString = 'Sie sind scheinbar nicht dazu berechtigt, eine der Stunden zu bearbeiten!';
      }
    }

    super.handleError(error);
  }

}
