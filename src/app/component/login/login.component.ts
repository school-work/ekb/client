import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../service/auth.service';
import {Router} from '@angular/router';
import {Credentials} from '../../models/Credentials';
import {HandleError} from '../../helper/handleError';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent extends HandleError implements OnInit {

  public credentials: Credentials = new Credentials();

  constructor(public auth: AuthService, private router: Router) {
    super();
  }

  ngOnInit() {
  }

  public doLogin(): void {
    document.getElementById('submit').setAttribute('disabled', 'disabled');

    this.auth.submitCredentials(this.credentials).subscribe(data => {
      this.auth.login(data['data']);
    }, error1 => {
      this.handleError(error1);
      console.log(error1);
      document.getElementById('submit').removeAttribute('disabled');
    });
  }

}
