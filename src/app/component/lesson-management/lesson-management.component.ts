import {Component, OnInit} from '@angular/core';
import {HandleError} from '../../helper/handleError';
import {HttpErrorResponse} from '@angular/common/http';
import {LessonService} from '../../service/lesson.service';
import {Lesson} from '../../models/Lesson';
import {Router} from '@angular/router';
import {AuthService} from '../../service/auth.service';

@Component({
  selector: 'app-class-management',
  templateUrl: './lesson-management.component.html',
  styles: []
})
export class LessonManagementComponent extends HandleError implements OnInit {

  public currentDate = new Date().toISOString().substr(0, 10);
  public lessons: Lesson[] = [];

  constructor(private lesson: LessonService, public router: Router, public auth: AuthService) {
    super();
  }

  ngOnInit() {
    this.onChange();
  }

  public onChange() {
    this.lesson.getLessonListByDate(this.currentDate).subscribe(data => {
      this.lessons = data['data'];
    }, error1 => {
      this.handleError(error1);
    });
  }
}
