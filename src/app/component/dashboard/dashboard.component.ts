import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../service/auth.service';

@Component({
  selector: 'app-callback',
  templateUrl: './dashboard.component.html'
})
export class DashboardComponent implements OnInit {
  constructor(public auth: AuthService) {
  }

  ngOnInit() {
  }
}
