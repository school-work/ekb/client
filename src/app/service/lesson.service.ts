import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {AuthService} from './auth.service';

@Injectable()
export class LessonService {

  private endPoint = environment.apiUrl + '/api/lesson';

  constructor(private http: HttpClient, private auth: AuthService) {
  }

  public getLessonListByDate(date: string) {
    return this.http.get(this.endPoint + 's/' + date, this.auth.httpOptions());
  }

  public getLessonByIdentifier(identifier: string) {
    return this.http.get(this.endPoint + '/' + identifier, this.auth.httpOptions());
  }

}
