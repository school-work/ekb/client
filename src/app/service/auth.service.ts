import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {environment} from '../../environments/environment';
import {Credentials, Token} from '../models/Credentials';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable()
export class AuthService implements CanActivate {

  private endPoint = environment.apiUrl + '/api/secure';
  public tokenValidate = false;

  constructor(public router: Router, private http: HttpClient) {
  }

  public submitCredentials(credentials: Credentials) {
    return this.http.post(this.endPoint + '/authenticate', credentials);
  }

  public login(token: Token): void {
    sessionStorage.setItem('shortName', token.LehrerID);
    sessionStorage.setItem('auth_token', token.Token);
    this.router.navigate(['/']);
  }

  public logout(): void {
    sessionStorage.clear();
    this.router.navigate(['/']);
  }

  public getToken(): string {
    return sessionStorage.getItem('auth_token');
  }

  public getName(): string {
    return sessionStorage.getItem('shortName');
  }

  public httpOptions() {
    if (this.getToken() != null && this.getName() != null) {
      return {
        headers: new HttpHeaders({
          'auth_token': this.getToken(),
          'auth_name': this.getName()
        })
      };
    } else {
      return {
        headers: new HttpHeaders()
      };
    }
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    this.http.get(this.endPoint + '/validate', this.httpOptions()).subscribe(value => {
      if (value['data'] == 'success') {
        this.tokenValidate = true;
      } else {
        this.tokenValidate = false;
        this.router.navigate(['/login']);
      }
    }, error1 => {
      this.tokenValidate = false;
      this.router.navigate(['/login']);
    });

    return true;
  }
}
