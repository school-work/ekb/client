import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {AuthService} from './auth.service';
import {Presence} from '../models/Presence';

@Injectable()
export class PresenceService {

  private endPoint = environment.apiUrl + '/api/presence';

  constructor(private http: HttpClient, private auth: AuthService) {
  }

  public getPresenceList() {
    return this.http.get(this.endPoint + '/list', this.auth.httpOptions());
  }

  public getPresenceListByLessonId(identifier: string) {
    return this.http.get(this.endPoint + '/list/' + identifier, this.auth.httpOptions());
  }

  public savePresenceEntry(presence: Presence[]) {
    return this.http.post(this.endPoint + '/save', presence, this.auth.httpOptions());
  }

}
