import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import {AppComponent} from './app.component';
import {RouterModule, Routes} from '@angular/router';
import {SiteNotFoundComponent} from './component/site-not-found/site-not-found.component';
import {AuthService} from './service/auth.service';
import {DashboardComponent} from './component/dashboard/dashboard.component';
import {LoginComponent} from './component/login/login.component';
import {AccessDeniedComponent} from './component/access-denied/access-denied.component';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {StripHtmlPipe} from './helper/strip-html.pipe';
import {TrimHtmlPipe} from './helper/trim-html.pipe';
import {FroalaEditorModule, FroalaViewModule} from 'angular-froala-wysiwyg';
import {PresenceAddComponent} from './component/presence/presence-add/presence-add.component';
import {PresenceService} from './service/presence.service';
import {LessonManagementComponent} from './component/lesson-management/lesson-management.component';
import {LessonService} from './service/lesson.service';
import {PresenceEditComponent} from './component/presence/presence-edit/presence-edit.component';

const appRoutes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    canActivate: [AuthService]
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'access-denied',
    component: AccessDeniedComponent
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [AuthService]
  },
  {
    path: 'presence/add',
    component: PresenceAddComponent,
    canActivate: [AuthService]
  },
  {
    path: 'presence/edit/:id',
    component: PresenceEditComponent,
    canActivate: [AuthService]
  },
  {
    path: 'lesson',
    component: LessonManagementComponent,
    canActivate: [AuthService]
  },
  {
    path: '**',
    component: SiteNotFoundComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    SiteNotFoundComponent,
    DashboardComponent,
    LoginComponent,
    AccessDeniedComponent,
    StripHtmlPipe,
    TrimHtmlPipe,
    PresenceAddComponent,
    LessonManagementComponent,
    PresenceEditComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(
      appRoutes,
      {enableTracing: true}
    ),
    NgbModule.forRoot(),
    HttpClientModule,
    FormsModule,
    FroalaEditorModule.forRoot(),
    FroalaViewModule.forRoot()
  ],
  providers: [
    AuthService,
    PresenceService,
    LessonService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
